# Desarrollo de Aplicaciones Web y Base de Datos
### Juan Alejandro Alcántara Minaya - A01703947

## Desarrollo de Aplicaciones Web

## Base de Datos
#### Diferencia entre DB (Base de datos) y DBMS (Gestor de base de datos)
Una __base de datos__ sólo se refiere al conjunto de archivos relacionados lógicamente que guardan la información.

Un __gestor de base de datos__ es el software que se encarga de manejar de manera correcta esta colección de archivos. Entre sus responsabilidades se incluyen:
  * Operaciones CRUD.
  * Seguridad y permisos.
  * Manejo de conexiones concurrentes.
  * Cumplimiento de restricciones.

Por si sola la base de datos no se puede usar sin complicarse la vida, es por esto que hay empresas, e.g. Oracle, que se dedican al desarrollo de estos programas para gestionar estos archivos y que a la hora de desarrollar nos podamos concentrar en otras cosas y no usar tiempo de implementación para la escritura y lectura de archivos.


