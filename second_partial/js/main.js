const SERVER_ROOT = "/second_partial";
// MATERIALIZE SETUP
$(document).ready(function(){
   $('.modal').modal();
});



//UTILS
var get_zombies = new Promise((res, rej) => {
  $.get(`${SERVER_ROOT}/actions/get_zombies.php`, function(response){
    res(JSON.parse(response));
  });
});
var get_zombie_states = function(id) {
  return new Promise((res, rej) => {
    let url = `${SERVER_ROOT}/actions/get_zombie_states.php`;
    if(id >= 0){
      url += `?id=${id}`;
    }
    $.get(url, function(response){
      res(JSON.parse(response));
    });
  });
}

var get_states = new Promise((res, rej) => {
  $.get(`${SERVER_ROOT}/actions/get_states.php`, function(response){
    res(JSON.parse(response));
  });
});

var update_zombie_states = function(zombie_id){
  get_zombie_states(zombie_id).then((states) => {
    $(states).each((idx, state) => {
      $(`#zombie_${zombie_id}`).siblings(".zombie_states").children("p").append(`${state.fecha} - ${state.nombre}<br>`);
    });
  });
}

var update_states = function(){
  get_states.then((states) => {
    $(states).each((idx, state) => {
      $("#zombie_state_options").append(`
        <option value="${state.id}">${state.nombre}</option>
      `);
    });
  });
}

var update_zombies = function(){
  get_zombies.then((zombies) => {
    $(zombies).each((idx, zombie) => {
      $("#zombie_rows").append(`
      <tr>
        <td id="zombie_${zombie.id}">${zombie.nombre}</td>
        <td class="zombie_states" data-id="${zombie.id}">
          <p></p>
          <a data-id="${zombie.id}" href="javascript:void(0)" class="add_state waves-effect waves-light btn"><i class="material-icons left">add</i>Registrar estado</a>
        </td>
      </tr>
      `);
      $(".add_state").on("click", function(){
        $('#zombie_state_id').val($(this).data("id"));
        $('#add_zombie_state').modal("open");
      });
      update_zombie_states(zombie.id);
    });
  });
}

var add_zombie = function(name){
  $.post(`${SERVER_ROOT}/actions/add_zombie.php`, {name: name}, function(response){
    update_zombies();
    console.log(JSON.parse(response).message);
  });
}


//ADD ZOMBIE FUNCTION
$("#add_zombie_submit").on("click", function(){
  let name = $("#add_zombie_nombre").val();
  add_zombie(name);
});


update_zombies();
update_states();
