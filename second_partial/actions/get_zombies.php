<?php
  require("../utils.php");
  $db = db_connect();

  $query = "SELECT id,nombre FROM Zombies;";

  $result = mysqli_query($db, $query);

  //IF IT FAILS RETURN 503...
  if(!$result){
    http_response_code(503);
    exit('{ "message": "Query execution failed." }');
  }

  echo json_encode(mysqli_fetch_all($result, MYSQLI_ASSOC));
  
  
  db_close($db);
?>
