<?php
  require("../utils.php");
  if(isset($_POST)){
    $db = db_connect();

    //PREPARE THE STATEMENT TO CREATE A ZOMBIE
    $add_zombie = mysqli_prepare($db, "INSERT INTO Zombies (nombre) VALUES (?)");

    //IF IT FAILS RETURN 503...
    if(!$add_zombie){
      db_close($db);
      http_response_code(503);
      exit('{ "message": "Query preparation failed." }');
    }

    //WE BIND THE PARAMTERS
    mysqli_stmt_bind_param($add_zombie, "s", $_POST["name"]);


    //WE EXECUTE THE PROCEDURE
    $exec = mysqli_stmt_execute($add_zombie);

    //IF IT FAILS RETURN 503...
    if(!$exec){
      db_close($db);
      http_response_code(503);
      exit('{ "message": "Query execution failed." }');
    }

    http_response_code(200);
    echo '{ "message": "Success." }';

    db_close($db);
  }
?>
