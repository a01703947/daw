DROP TABLE IF EXISTS Zombies;
CREATE TABLE Zombies (
    ID int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(200)
);

DROP TABLE IF EXISTS TiposEstado;
CREATE TABLE TiposEstado (
    ID int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(200)
);

DROP TABLE IF EXISTS Estados;
CREATE TABLE Estados (
    ID int AUTO_INCREMENT PRIMARY KEY,
    fecha TIMESTAMP DEFAULT NOW(),
    zombie int,
    tipo int,
    FOREIGN KEY (zombie) REFERENCES Zombies(ID) ON DELETE CASCADE,
    FOREIGN KEY (tipo) REFERENCES TiposEstado(ID) ON DELETE CASCADE
);

INSERT INTO TiposEstado (nombre) VALUES ('infeccion');
INSERT INTO TiposEstado (nombre) VALUES ('desorientacion');
INSERT INTO TiposEstado (nombre) VALUES ('violencia');
INSERT INTO TiposEstado (nombre) VALUES ('desmayo');
INSERT INTO TiposEstado (nombre) VALUES ('transformacion');
