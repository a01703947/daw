CREATE TABLE Materiales(
    Clave NUMERIC(5) NOT NULL,
    Descripcion VARCHAR(50),
    Costo NUMERIC(8,2),
    PRIMARY KEY (Clave)
);

CREATE TABLE Proveedores(
    RFC VARCHAR(13) NOT NULL,
    RazonSocial VARCHAR(50),
    PRIMARY KEY (RFC)
);

CREATE TABLE Proyectos(
    Numero INT,
    Denominacion VARCHAR(50),
    PRIMARY KEY (Numero)
);

CREATE TABLE Entregan(
    Clave NUMERIC(5),
    RFC VARCHAR(13),
    Numero INT,
    Fecha DATE,
    Cantidad INT,
    FOREIGN KEY (Clave) REFERENCES Materiales(Clave) ON DELETE CASCADE,
    FOREIGN KEY (RFC) REFERENCES Proveedores(RFC) ON DELETE CASCADE,
    FOREIGN KEY (Numero) REFERENCES Proyectos(Numero) ON DELETE CASCADE,
    PRIMARY KEY (Clave, RFC, Numero, Fecha)
);

sp_help Materiales;
sp_help Proveedores;
sp_help Proyectos;
sp_help Entregan;

DROP TABLE Entregan;
DROP TABLE Materiales;
DROP TABLE Proveedores;
DROP TABLE Proyectos;

SELECT * FROM sysobjects where xtype='U';

SELECT * FROM Materiales;
SELECT * FROM Proveedores;
SELECT * FROM Proyectos;
SELECT * FROM Entregan;
