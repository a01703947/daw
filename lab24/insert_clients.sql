BEGIN;
  INSERT INTO ClientesBanca VALUES('001', 'Manuel Rios Maldonado', 9000);
  INSERT INTO ClientesBanca VALUES('002', 'Pablo Perez Ortiz', 5000);
  INSERT INTO ClientesBanca VALUES('003', 'Luis Flores Alvarado', 5000);
COMMIT;
SELECT * FROM ClientesBanca;

/* 
 * ¿Que pasa cuando deseas realizar esta consulta? 
 *    Se muestran los nuevos registros en ambas conexiones.
 */
