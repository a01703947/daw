BEGIN;
  INSERT INTO ClientesBanca VALUES('005','Rosa Ruiz Maldonado',9000);
  INSERT INTO ClientesBanca VALUES('006','Luis Camino Ortiz',5000);
  INSERT INTO ClientesBanca VALUES('001','Oscar Perez Alvarado',8000);
-- PostgreSQL automaticamente hace ROLLBACK si hubo un error.
END; -- Lo mismo que COMMIT.

/* ¿PARA QUÉ SIRVE EL COMANDO @@ERROR REVISA LA AYUDA EN LÍNEA?
 *  Ayuda identificar si hubo un error en la ejecucción del query anterior.
 * */

/* ¿QUÉ HACE LA TRANSACCIÓN?
 *  Trata de crear nuevos clientes pero existe una llave duplicada. Como hubo un
 *  error de integridad existe un error entonces hace un ROLLBACK.
 * */

/* ¿HUBO ALGUNA MODIFICACIÓN EN LA TABLA? EXPLICA QUÉ PASÓ Y POR QUÉ
 *  No. Como hubo un error de integridad existe un error entonces hace 
 *  un ROLLBACK.
 * */

