BEGIN;
  INSERT INTO ClientesBanca VALUES('004','Ricardo Rios Maldonado',19000);
  INSERT INTO ClientesBanca VALUES('005','Pablo Ortiz Arana',15000);
  INSERT INTO ClientesBanca VALUES('006','Luis Manuel Alvarado',18000);

SELECT * FROM ClientesBanca;

/* 
 * ¿QUE PASA CUANDO DESEAS REALIZAR ESTA CONSULTA? 
 *   En la primera sesión me muestra los nuevos registros porque agarra el SELECT
 *   como parte de la transacción mientras que en la segunda sesión sólo se 
 *   muestran los registros previos.
 */


/* 
 * EXPLICA PORQUE OCURRE DICHO EVENTO
 *  Creo que se espera que se bloquee el recurso pero en el caso de PostgreSQL
 *  simplemente asume que la instrucción nunca se realizo, o realiza un ROLLBACK
 *  implícito.
 */

/* 
 * ¿QUÉ OCURRIÓ Y POR QUÉ?
 *  No ocurrió nada diferente después de emitir el ROLLBACK porque ya lo tiene
 *  implícito PostgreSQL
 */
